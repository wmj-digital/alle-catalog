# alle-catalog

Display products variants from Alle* marketplace. Catalog allow extend range of marketing campaigns. Show your products, get clients and redirect them to your offer.

![Catalog page.](./printscreen.png)

* Before use, registry app in API Alle* for get api credentials.
* After registration, you must get access token and renew token manually.
But only at first initial app run. Later app will automatically renew token.
* Write credentials to .env file.
* In order to refresh cached catalog page add Lumen/Laravel scheduler to server CRON. App will refresh catalog cache daily.

To correctly work offer description should have the fallowing structure:
* First paragraph can hold any content, is skipped.
* Second paragraph must have TEXT section with offer title surrounded by \<h2> tag, and have short description as normal text.
* Next paragraphs con be anything, are skipped.
 
[Api registration](https://developer.allegro.pl/auth/#device-flow)

[Laravel task scheduling](https://laravel.com/docs/8.x/scheduling)
 
## Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Contributing

Thank you for considering contributing to Lumen! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

