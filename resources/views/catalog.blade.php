
<!doctype html>
<html lang="pl">
<head>
    <link rel="icon" href="https://sklep.forkt.pl/wp-content/uploads/2021/05/workshop_favicon-color-light-theme-512px-1.svg" sizes="192x192">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Katalog | Pracownia Forkt</title>

    <style>
        .innerHeader h2{
            font-size: large;
        }
        .innerHeader p{
            font-size: smaller;
        }
    </style>
</head>
<body style="background-color: whitesmoke">
<nav class="w-100 p-1 mb-5 shadow position-sticky fixed-top" style="background-color: white; max-height: 8vmin">
    <div class="navbar-brand" style="height: 7vmin; margin-left: 3vw">
        <img style="max-height: 100%; max-width: 100%" src="https://sklep.forkt.pl/wp-content/uploads//2021/05/workshop_logo-color-light_theme.svg" alt="Pracownia Forkt">
    </div>
</nav>

@if (isset($catalog))
    <main class="container">
    @foreach($catalog as $list)
        <section class="w-100 align-middle mt-5" >

        <h3 style="color: #2e3136">{{ $catalog->key() }} </h3>
            <div class="d-flex flex-wrap justify-content-sm-around">
                @foreach($list as $productDetails)
                    <article class="shadow-sm card mb-3 small p-3" style="min-width: 300px; width: 48%; margin: 1%; background-color: white; box-sizing: border-box">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img src="{{ $productDetails->getPhotoUrl() }}" alt="Image" class="img-fluid rounded-start">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
{{--                                    <h6 class="card-title">{{ $productDetails->getTitle() }}</h6>--}}
                                    <div class="card-text font-weight-light innerHeader">{!! $productDetails->getFirstDescriptionParagraph() !!}</div>
                                    <a href="{{ $productDetails->getOfferUrl() }}" class="btn btn-sm btn-outline-secondary align-middle">Otwórz w <span style="color: darkorange">Allegro</span></a>
                                </div>
                            </div>
                        </div>
                    </article>
                @endforeach
            </div>
        </section>
    @endforeach
    </main>
@endif

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
