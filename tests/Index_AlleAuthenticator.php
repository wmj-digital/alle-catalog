<?php


interface Index_AlleAuthenticator
{
    const ADAPTER_METHODS = [
        self::M_SERIALIZED_AUTH_STATE_FILE_EXIST,
        self::M_GET_AUTH_STATE_FROM_STORAGE,
        self::M_GET_USER_ID,
        self::M_GET_USER_SECRET,
        self::M_GET_ENV_ACCESS_TOKEN,
        self::M_GET_ENV_REFRESH_TOKEN,
        self::M_PUT_AUTH_STATE_TO_STORAGE,
        self::M_REQUEST_FOR_NEW_TOKEN
    ];

    const M_SERIALIZED_AUTH_STATE_FILE_EXIST = 'serializedAuthStateFileExist';
    const M_UNSERIALIZE = 'unserialize';
    const M_DECODE_AUTH_STATE = 'decodeAuthState';
    const M_GET_AUTH_STATE_FROM_STORAGE = 'getAuthStateFromStorage';
    const M_GET_USER_ID = 'getUserId';
    const M_GET_USER_SECRET = 'getUserSecret';
    const M_GET_ENV_ACCESS_TOKEN = 'getEnvAccessToken';
    const M_GET_ENV_REFRESH_TOKEN = 'getEnvRefreshToken';
    const M_PUT_AUTH_STATE_TO_STORAGE = 'putAuthStateToStorage';
    const M_SERIALIZE = 'serialize';
    const M_GET_TOKEN = 'getToken';
    const M_TOKEN_EXPIRED = 'tokenExpired';
    const M_HOURS_SINCE_LAST_TOKEN_REFRESH = 'hoursSinceLastTokenRefresh';
    const M_GET_CURRENT_DATE_TIME = 'getCurrentDateTime';
    const M_REFRESH_ACCESS_TOKEN = 'refreshAccessToken';
    const M_REQUEST_FOR_NEW_TOKEN = 'requestForNewToken';
    const M_INIT_STATE = 'initState';

    const P_LAST_TOKEN_REFRESH = 'lastTokenRefresh';
}
