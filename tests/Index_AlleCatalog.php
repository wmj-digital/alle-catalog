<?php
namespace Test;

interface Index_AlleCatalog
{
    const M_CURRENT = 'current';
    const M_CREATE_PRODUCT_DETAILS = 'createProductDetails';
    const M_NEXT = 'next';
    const M_KEY = 'key';
    const M_VALID = 'valid';
    const M_REWIND = 'rewind';
}
