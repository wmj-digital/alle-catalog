<?php

namespace Wmj\UserManager\Tests;

use ReflectionException;
use ReflectionMethod;

class AccessibleMethod
{

    protected object $object;
    protected ReflectionMethod $method;

    /**
     * @param object $object
     * @param string $method
     *
     * @throws ReflectionException
     */
    function __construct(object $object, string $method)
    {
        $this->object = $object;
        $this->method = $this->returnAccessibleReflectedMethod($object, $method);
    }

    /**
     * @param        $class
     * @param string $method
     *
     * @return ReflectionMethod
     * @throws ReflectionException
     */
    function returnAccessibleReflectedMethod(object $class, string $method): ReflectionMethod
    {
        $reflectedMethod = new ReflectionMethod($class, $method);
        $reflectedMethod->setAccessible(true);
        return $reflectedMethod;
    }

    /**
     * @param mixed ...$args
     *
     * @return mixed
     */
    function call(...$args)
    {
        return empty($args) ? $this->method->invoke($this->object) : $this->method->invokeArgs($this->object, ... $args);
    }
}
