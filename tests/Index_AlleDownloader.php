<?php
namespace Tests;

interface Index_AlleDownloader
{
    const M_GET_VARIANT_LIST = 'getVariantList';
    const M_GET_VARIANT_SET = 'getVariantSet';
    const M_GET_PRODUCT_DETAILS = 'getProductDetails';
    const M_GET_VARIANT_DETAILS_URL = 'getVariantDetailsUrl';
}
