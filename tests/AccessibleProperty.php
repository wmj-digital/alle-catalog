<?php

namespace Wmj\UserManager\Tests;

use ReflectionException;
use ReflectionProperty;

class AccessibleProperty
{
    protected object $object;
    protected ReflectionProperty $property;

    /**
     * @param object $object
     * @param string $property
     *
     * @throws ReflectionException
     */
    function __construct(object $object, string $property)
    {
        $this->object = $object;
        $this->property = $this->returnAccessibleReflectedProperty($object, $property);
    }

    /**
     * @param        $class
     * @param string $property
     *
     * @return ReflectionProperty
     * @throws ReflectionException
     */
    function returnAccessibleReflectedProperty(object $class, string $property): ReflectionProperty
    {
        $reflectedProperty = new ReflectionProperty($class, $property);
        $reflectedProperty->setAccessible(true);
        return $reflectedProperty;
    }

    function get()
    {
        return $this->property->getValue($this->object);
    }

    function set($value)
    {
        $this->property->setValue($this->object, $value);
    }
}