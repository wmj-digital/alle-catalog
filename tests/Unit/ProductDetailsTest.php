<?php


use App\Models\AlleDownloader;
use App\Models\ProductDetails;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\Index_AlleDownloader;

class ProductDetailsTest extends TestCase
{
    const TEST_PRODUCT_DETAILS = [
        ProductDetails::ID => '12374',
        ProductDetails::NAME => 'test product',
        ProductDetails::IMAGES => [
            'first_image_url',
            'second_image_url',
            'third_image_url'
        ],
        ProductDetails::DESCRIPTION => [
            ProductDetails::DESCRIPTION_SECTIONS => [

            ]
        ]
    ];


    /**
     * @var AlleDownloader|MockObject
     */
    protected $downloaderMock;

    function setUp(): void
    {
        $this->downloaderMock = $this->createMock(AlleDownloader::class);
        $this->downloaderMock
            ->method(Index_AlleDownloader::M_GET_PRODUCT_DETAILS)
            ->willReturn(self::TEST_PRODUCT_DETAILS);
    }

    function test_download_data_during_instance_creation(): void
    {
        $productId = 'fghn';
        $this->downloaderMock
            ->expects($this->once())
            ->method(Index_AlleDownloader::M_GET_PRODUCT_DETAILS)->with($productId);

        $this->createProductDetails($productId);
    }

    function createProductDetails(string $id = ''): ProductDetails
    {
        return (new ProductDetails($this->downloaderMock))->downloadDetails($id);
    }

    function test_getTitle(): void
    {
        $this->assertEquals(self::TEST_PRODUCT_DETAILS[ProductDetails::NAME], $this->createProductDetails()->getTitle());
    }

//    /**
//     * @return string
//     */
//    function getFirstDescriptionParagraph(): string
//    {
//        return $this->productDetails[self::DESCRIPTION][self::DESCRIPTION_SECTIONS]['items'];
//    }

    function test_getOfferUrl(): void
    {
        $expected = ProductDetails::ALLE_OFFER_URL_BASE . self::TEST_PRODUCT_DETAILS[ProductDetails::ID];
        $this->assertEquals($expected, $this->createProductDetails()->getOfferUrl());
    }

    function test_getFirstPhotoUrl(): void
    {
        $this->assertEquals(self::TEST_PRODUCT_DETAILS[ProductDetails::IMAGES][0], $this->createProductDetails()->getPhotoUrl());
    }
}
