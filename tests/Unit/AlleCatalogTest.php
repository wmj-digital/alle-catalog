<?php


use App\Models\AlleCatalog;
use App\Models\AlleDownloader;
use App\Models\ProductDetails;
use PHPUnit\Framework\MockObject\MockObject;
use Test\Index_AlleCatalog;
use Tests\Index_AlleDownloader;

class AlleCatalogTest extends TestCase
{
    const TEST_VARIANT_SETS = [
        [
            AlleCatalog::VARIANT_ID => '123',
            AlleCatalog::OFFERS => [
                [AlleCatalog::OFFER_ID => 'offer1_id'],
                [AlleCatalog::OFFER_ID => 'offer3_id']
            ],
            AlleCatalog::VARIANT_NAME => 'variant1'

        ],
        [
            AlleCatalog::VARIANT_ID => '321',
            AlleCatalog::OFFERS => [
                AlleCatalog::OFFER_ID => 'offer2_id'
            ],
            AlleCatalog::VARIANT_NAME => 'variant1'
        ],
    ];

    /**
     * @var AlleDownloader|MockObject
     */
    protected $downloaderMock;
    /**
     * @var MockObject | AlleCatalog
     */
    protected MockObject $catalog;

    function setUp(): void
    {
        $this->downloaderMock = $this->createMock(AlleDownloader::class);
        $this->downloaderMock
            ->method(Index_AlleDownloader::M_GET_VARIANT_LIST)
            ->willReturn(self::TEST_VARIANT_SETS);
    }

    function test_download_variant_list_during_instance_creation(): void
    {
        $this->downloaderMock
            ->expects($this->once())
            ->method(Index_AlleDownloader::M_GET_VARIANT_LIST);

        $this->createCatalog();
    }

    function createCatalog(): AlleCatalog
    {
        return new AlleCatalog($this->downloaderMock);
    }

    function test_iteration_through_variant_list_count(): void
    {
        $catalog = $this->createCatalog();
        $iterationCounter = 0;

        foreach ($catalog as $variant) {
            $iterationCounter++;

            if ($iterationCounter > count(self::TEST_VARIANT_SETS)) $this->fail('To many loops.');
        }

        $this->assertEquals(count(self::TEST_VARIANT_SETS), $iterationCounter);
    }

    /**
     * Iterating over variants list, variant name is used as catalog key.
     */
    function test_iterating_over_variant_list(): void
    {
        $catalog = $this->createCatalog();
        for ($i = 0; $i < count(self::TEST_VARIANT_SETS); $i++) {
            $this->assertEquals(self::TEST_VARIANT_SETS[$i][AlleCatalog::VARIANT_NAME], $catalog->key());
            $catalog->next();
        }
    }

    /**
     * For every list, catalog return ProductDetails created by generator
     */
    function test_return_generator_with_variant_list_offers(): void
    {
        $this->catalog = $this->getMockBuilder(AlleCatalog::class)
            ->onlyMethods([Index_AlleCatalog::M_CREATE_PRODUCT_DETAILS])
            ->setConstructorArgs([$this->downloaderMock])
            ->getMock();

        $detailsMock = $this->getMockBuilder(ProductDetails::class)
            ->setConstructorArgs([$this->downloaderMock])
            ->getMock();

        $this->catalog->method(Index_AlleCatalog::M_CREATE_PRODUCT_DETAILS)->willReturn($detailsMock);
        $this->downloaderMock->method(Index_AlleDownloader::M_GET_VARIANT_LIST)->willReturn(self::TEST_VARIANT_SETS);

        $generator = $this->catalog->current();
        $this->assertInstanceOf(Generator::class, $generator);

        $detailsMock->method('downloadDetails')->withConsecutive([
            self::TEST_VARIANT_SETS[0][AlleCatalog::OFFERS][0][AlleCatalog::OFFER_ID],
            self::TEST_VARIANT_SETS[0][AlleCatalog::OFFERS][1][AlleCatalog::OFFER_ID]
        ]);

        iterator_to_array($generator);
    }
}
