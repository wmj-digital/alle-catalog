<?php


use App\Models\AlleAuthenticator;
use PHPUnit\Framework\MockObject\MockObject;
use Wmj\UserManager\Tests\AccessibleMethod;
use Wmj\UserManager\Tests\AccessibleProperty;

class AlleAuthenticatorTest extends TestCase
{
    /**
     * @var MockObject | AlleAuthenticator
     */
    protected MockObject $auth;
    /**
     * @var false|string
     */
    protected $serializedState;

    function setUp(): void
    {
        $this->auth = $this->getMockBuilder(AlleAuthenticator::class)
            ->onlyMethods(Index_AlleAuthenticator::ADAPTER_METHODS)
            ->getMock();

        $this->serializedState = json_encode([
            AlleAuthenticator::ACCESS_TOKEN_OFFSET => 'bvvvtt',
            AlleAuthenticator::USER_ID_OFFSET => 'user',
            AlleAuthenticator::USER_SECRET_OFFSET=> 'secret',
            AlleAuthenticator::REFRESH_TOKEN_OFFSET => 'ewrtj',
            AlleAuthenticator::LAST_TOKEN_REFRESH_OFFSET => 0
        ]);
    }

    /**
     * @throws ReflectionException
     */
    function test_when_serialized_state_not_exist_load_from_env(): void
    {
        $this->auth->method(Index_AlleAuthenticator::M_SERIALIZED_AUTH_STATE_FILE_EXIST)->willReturn(false);

        $this->auth->expects($this->once())->method(Index_AlleAuthenticator::M_GET_ENV_REFRESH_TOKEN);
        $this->auth->expects($this->once())->method(Index_AlleAuthenticator::M_GET_ENV_ACCESS_TOKEN);
        $this->auth->expects($this->once())->method(Index_AlleAuthenticator::M_GET_USER_ID);
        $this->auth->expects($this->once())->method(Index_AlleAuthenticator::M_GET_USER_SECRET);

        $m_initState = new AccessibleMethod($this->auth, Index_AlleAuthenticator::M_INIT_STATE);
        $m_initState->call();
    }

    /**
     * @throws ReflectionException
     */
    function test_when_serialized_state_exist_load_it(): void
    {
        $this->auth->method(Index_AlleAuthenticator::M_SERIALIZED_AUTH_STATE_FILE_EXIST)->willReturn(true);

        $this->auth->method(Index_AlleAuthenticator::M_GET_AUTH_STATE_FROM_STORAGE)->willReturn($this->serializedState);

        $this->auth->expects($this->once())->method(Index_AlleAuthenticator::M_GET_AUTH_STATE_FROM_STORAGE);

        (new AccessibleMethod($this->auth, Index_AlleAuthenticator::M_INIT_STATE))->call();
    }


    /**
     * @param bool     $tokenExpiredFlag
     * @param callable $expectation
     *
     * @dataProvider tokenRefreshScenario
     */
    function test_refresh_token_only_when_validity_time_expired(bool $tokenExpiredFlag, callable $expectation): void
    {
        $this->auth = $this->getMockBuilder(AlleAuthenticator::class)
            ->onlyMethods([
                Index_AlleAuthenticator::M_TOKEN_EXPIRED,
                Index_AlleAuthenticator::M_REFRESH_ACCESS_TOKEN,
                ...Index_AlleAuthenticator::ADAPTER_METHODS])
            ->getMock();

        $this->auth->method(Index_AlleAuthenticator::M_TOKEN_EXPIRED)->willReturn($tokenExpiredFlag);
        $expectation($this->auth);

        $this->auth->getToken();
    }

    /**
     * @return Generator
     */
    function tokenRefreshScenario(): Generator
    {
        yield [false, fn(MockObject $mockObject) => $mockObject->expects($this->never())->method(Index_AlleAuthenticator::M_REFRESH_ACCESS_TOKEN)];
        yield [true, fn(MockObject $mockObject) => $mockObject->expects($this->once())->method(Index_AlleAuthenticator::M_REFRESH_ACCESS_TOKEN)];
    }

    /**
     * @param DateTime $dateTime
     * @param callable $assertion
     *
     * @throws ReflectionException
     * @dataProvider tokenLiveTime
     */
    function test_validity_expire_when_will_exceed_live_time(DateTime $dateTime, callable $assertion):void{
        $m_tokenExpired = new AccessibleMethod($this->auth, Index_AlleAuthenticator::M_TOKEN_EXPIRED);
        $p_lastTokenRefresh = new AccessibleProperty($this->auth, Index_AlleAuthenticator::P_LAST_TOKEN_REFRESH);

        $p_lastTokenRefresh->set($dateTime);
        $assertion($m_tokenExpired->call());
    }

    function tokenLiveTime():Generator{
        yield [(new DateTime())->setTimestamp(0), fn(bool $validity) => $this->assertTrue($validity)];
        yield [(new DateTime())->add(new DateInterval('PT6H')), fn(bool $validity) => $this->assertFalse($validity)];
        yield [(new DateTime())->add(new DateInterval('PT24H')), fn(bool $validity) => $this->assertTrue($validity)];
    }
}
