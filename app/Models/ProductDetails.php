<?php


namespace App\Models;


class ProductDetails
{
    const ID = 'id';
    const NAME = 'name';
    const IMAGES = 'images';
    const DESCRIPTION = 'description';
    const DESCRIPTION_SECTIONS = 'sections';
    const ALLE_OFFER_URL_BASE = 'https://allegro.pl/oferta/';

    protected AlleDownloader $downloader;
    protected string $offerId;
    protected array $productDetails;

    /**
     * ProductDetails constructor.
     *
     * @param AlleDownloader $downloader
     */
    function __construct(AlleDownloader $downloader)
    {
        $this->downloader = $downloader;
    }

    /**
     * @param string $offerId
     */
    function downloadDetails(string $offerId): self
    {
        $this->offerId = $offerId;
        $this->productDetails = $this->downloader->getProductDetails($this->offerId);

        return $this;
    }

    /**
     * @return string
     */
    function getTitle(): string
    {
        return $this->productDetails[self::NAME];
    }

    /**
     * @return string
     */
    function getFirstDescriptionParagraph(): string
    {
//        $firstParagraph = $this->productDetails[self::DESCRIPTION][self::DESCRIPTION_SECTIONS][1]['items'];
//        array_key_exists('content', $firstParagraph[1])
        return array_key_exists('content', $this->productDetails[self::DESCRIPTION][self::DESCRIPTION_SECTIONS][1]['items'][1])
            ? $this->productDetails[self::DESCRIPTION][self::DESCRIPTION_SECTIONS][1]['items'][1]['content']
            : '';
//        return $this->productDetails[self::DESCRIPTION][self::DESCRIPTION_SECTIONS][1]['items'][1]['content'];
    }

    /**
     * @return string
     */
    function getOfferUrl(): string
    {
        return self::ALLE_OFFER_URL_BASE . $this->productDetails[self::ID];
    }

    /**
     * @return string
     */
    function getPhotoUrl(): string
    {
        return $this->productDetails[self::IMAGES][0];
    }
}
