<?php


namespace App\Models;

use Generator;
use Illuminate\Database\Eloquent\Model;
use Iterator;


class AlleCatalog extends Model implements Iterator
{
    const VARIANT_ID = 'id';
    const VARIANT_NAME = 'name';
    const OFFERS = 'offers';
    const OFFER_ID = 'id';

    protected AlleDownloader $downloader;

    protected array $variants;

    function __construct(AlleDownloader $downloader)
    {
        parent::__construct();

        $this->downloader = $downloader;
        $this->variants = $this->downloader->getVariantList();
    }

    public function current(): Generator
    {
//        var_dump(current($this->variants)[AlleDownloader::VARIANT_LIST_VARIANTS][AlleDownloader::VARIANT_ID]);
//        $variantSet = $this->downloader->getVariantList(current($this->variants)[AlleDownloader::VARIANT_LIST_VARIANTS][AlleDownloader::VARIANT_ID]);


        foreach (current($this->variants)[self::OFFERS] as $offer) {
            yield $this->createProductDetails($offer[self::OFFER_ID]);
        }
    }

    protected function createProductDetails(string $id): ProductDetails
    {
        return (new ProductDetails($this->downloader))->downloadDetails($id);
    }

    function next(): void
    {
        next($this->variants);
    }

    function key(): string
    {
        return current($this->variants)[self::VARIANT_NAME];
    }

    function valid(): bool
    {
        return key($this->variants) !== null;
    }

    function rewind(): void
    {
        reset($this->variants);
    }
}
