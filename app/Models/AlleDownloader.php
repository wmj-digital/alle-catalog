<?php


namespace App\Models;


use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
use RuntimeException;

class AlleDownloader
{
    const VARIANT_LIST_COUNT = 'count';
    const VARIANT_LIST_VARIANTS = 'offerVariants';
    const VARIANT_ID = 'id';
    const VARIANT_NAME = 'name';

    protected const URL_BASE = 'https://api.allegro.pl';
    protected const VARIANT_LIST_ENDPOINT_URL = self::URL_BASE . '/sale/offer-variants';
    protected const VARIANT_DETAILS_ENDPOINT_URL = self::URL_BASE . '/sale/offer-variants';
    protected const PRODUCT_DETAILS_ENDPOINT_URL = self::URL_BASE . '/sale/product-offers';

    protected string $apiToken = '';

    /**
     * AlleDownloader constructor.
     *
     * @param AlleAuthenticator $alleAuthenticator
     */
    function __construct(AlleAuthenticator $alleAuthenticator)
    {
        $this->setApiToken($alleAuthenticator->getToken());
    }

    /**
     * @param string $token
     *
     * @return $this
     */
    function setApiToken(string $token): self
    {
        $this->apiToken = $token;
        return $this;
    }

    /**
     * @return array
     */
    function getVariantList(): array
    {
        return array_map(
            fn(array $variant): array => $this->returnVariantDetails($variant[self::VARIANT_ID]),
            $this->returnVariantList()['offerVariants']
        );
    }

    /**
     * @param string $variantId
     *
     * @return array
     */
    protected function returnVariantDetails(string $variantId): array
    {
        $variantDetailsResponse = $this->downloadVariantDetails($variantId);
        if ($variantDetailsResponse->failed()) throw new RuntimeException("Variant with id $variantId details download fail.");

        return $variantDetailsResponse->json();
    }

    /**
     * @param string $variantId
     *
     * @return Response
     */
    protected function downloadVariantDetails(string $variantId): Response
    {
        return Http::withToken($this->apiToken)->get(self::VARIANT_DETAILS_ENDPOINT_URL . '/' . $variantId);
    }

    /**
     * @return array
     */
    protected function returnVariantList(): array
    {
        $listResponse = $this->downloadVariantList();
        if ($listResponse->failed()) throw new RuntimeException('Variant list download fail');

        return $listResponse->json();
    }

    /**
     * @return Response
     */
    protected function downloadVariantList(): Response
    {
        return Http::withToken($this->apiToken)->get(self::VARIANT_LIST_ENDPOINT_URL);
    }

    /**
     * @param string $offerId
     *
     * @return array
     */
    function getProductDetails(string $offerId): array
    {
        $detailsResponse = $this->downloadProductDetails($offerId);
        if ($detailsResponse->failed()) throw new RuntimeException("Download offer $offerId details fail.");

        return $detailsResponse->json();
    }


    /**
     * @param string $offerId
     *
     * @return Response
     */
    protected function downloadProductDetails(string $offerId): Response
    {
        return Http::withToken($this->apiToken)->withHeaders([
           "Accept: application/vnd.allegro.beta.v2+json"
        ])->get("https://api.allegro.pl/sale/product-offers/$offerId");
    }
}
