<?php


namespace App\Models;


use DateTime;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Exception;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use RuntimeException;
use Serializable;

class AlleAuthenticator extends Model implements Serializable
{
    const RESPONSE_ACCESS_TOKEN_OFFSET = 'access_token';
    const RESPONSE_REFRESH_TOKEN_OFFSET = 'refresh_token';
    const TOKEN_VALIDITY_IN_HOURS = 12;
    const AUTH_PATH = 'alle_auth.json';
    protected const LOCAL_STORAGE = 'local';
    const AUTH_KEY_OFFSET = 'auth_key';
    const ACCESS_TOKEN_OFFSET = 'access_token';
    const REFRESH_TOKEN_OFFSET = 'refresh_token';
    const LAST_TOKEN_REFRESH_OFFSET = 'last_token_refresh';
    const USER_ID_OFFSET = 'user_id';
    const USER_SECRET_OFFSET = 'user_secret';
    protected string $userId;
    protected string $userSecret;
    protected string $token;
    protected string $refreshToken;
    protected ?DateTime $lastTokenRefresh = null;

    /**
     * AlleAuthenticator constructor.
     *
     * @param array $attributes
     *
     * @throws FileNotFoundException
     * @throws Exception
     */
    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->initState();
    }

    /**
     * @throws FileNotFoundException
     */
    protected function initState(): void
    {
        if ($this->serializedAuthStateFileExist()) {
            $this->unserialize($this->getAuthStateFromStorage());
        } else {
            $this->userId = $this->getUserId();
            $this->userSecret = $this->getUserSecret();
            $this->token = $this->getEnvAccessToken();
            $this->refreshToken = $this->getEnvRefreshToken();
        }
    }

    /**
     * Adapter method.
     *
     * @return bool
     */
    protected function serializedAuthStateFileExist(): bool
    {
        return Storage::disk(self::LOCAL_STORAGE)->exists(self::AUTH_PATH);
    }

    /**
     * @param $serialized
     *
     * @throws Exception
     */
    public function unserialize($serialized): void
    {
        $data = $this->decodeAuthState($serialized);

        $this->userId = $data[self::USER_ID_OFFSET];
        $this->userSecret = $data[self::USER_SECRET_OFFSET];
        $this->token = $data[self::ACCESS_TOKEN_OFFSET];
        $this->refreshToken = $data[self::REFRESH_TOKEN_OFFSET];
        $this->lastTokenRefresh = $this->getCurrentDateTime()->setTimestamp($data[self::LAST_TOKEN_REFRESH_OFFSET]);
    }

    /**
     * Method for control input and output types.
     *
     * @param string $serializedState
     *
     * @return array
     */
    protected function decodeAuthState(string $serializedState): array
    {
        return json_decode($serializedState, true);
    }

    /**
     * @return DateTime
     */
    protected function getCurrentDateTime(): DateTime
    {
        return new DateTime();
    }

    /**
     * Adapter method.
     *
     * @return string
     * @throws FileNotFoundException
     */
    protected function getAuthStateFromStorage(): string
    {
        return Storage::disk(self::LOCAL_STORAGE)->get(self::AUTH_PATH);
    }

    /**
     * Adapter method.
     *
     * @return string
     */
    protected function getUserId(): string
    {
        return env('ALLE_USER_ID');
    }

    /**
     * Adapter method.
     *
     * @return string
     */
    protected function getUserSecret(): string
    {
        return env('ALLE_USER_SECRET');
    }

    /**
     * Adapter method.
     *
     * @return string
     */
    protected function getEnvAccessToken(): string
    {
        return env('ALLE_ACCESS_TOKEN');
    }

    /**
     * Adapter method.
     *
     * @return string
     */
    protected function getEnvRefreshToken(): string
    {
        return env('ALLE_REFRESH_TOKEN');
    }

    /**
     *
     */
    function __destruct()
    {
        $this->putAuthStateToStorage($this->serialize());

    }

    /**
     * Adapter method.
     *
     * @param string $authState
     */
    protected function putAuthStateToStorage(string $authState): void
    {
        Storage::disk(self::LOCAL_STORAGE)->put(self::AUTH_PATH, $authState);
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return json_encode([
            self::USER_ID_OFFSET => $this->userId,
            self::USER_SECRET_OFFSET => $this->userSecret,
            self::ACCESS_TOKEN_OFFSET => $this->token,
            self::REFRESH_TOKEN_OFFSET => $this->refreshToken,
            self::LAST_TOKEN_REFRESH_OFFSET => is_null($this->lastTokenRefresh) ? 0 : $this->lastTokenRefresh->getTimestamp()
        ]);
    }

    /**
     * @return string
     */
    function getToken(): string
    {
        if ($this->tokenExpired()) $this->refreshAccessToken();

        return $this->token;
    }

    /**
     * @return bool
     */
    protected function tokenExpired(): bool
    {
        return is_null($this->lastTokenRefresh) || $this->hoursSinceLastTokenRefresh() >= self::TOKEN_VALIDITY_IN_HOURS;
    }

    /**
     * @return int
     */
    protected function hoursSinceLastTokenRefresh(): int
    {
        return $this->lastTokenRefresh->diff($this->getCurrentDateTime())->h;
    }

    /**
     *
     */
    protected function refreshAccessToken(): void
    {
        $response = $this->requestForNewToken();
        if ($response->failed()) throw new RuntimeException('Requesting for new token fail.');

        $this->token = $response->offsetGet(self::RESPONSE_ACCESS_TOKEN_OFFSET);
        $this->refreshToken = $response->offsetGet(self::RESPONSE_REFRESH_TOKEN_OFFSET);
        $this->lastTokenRefresh = $this->getCurrentDateTime();

        $response->close();
    }

    /**
     * Adapter method.
     *
     * @return Response
     */
    protected function requestForNewToken(): Response
    {
        return Http::withBasicAuth($this->userId, $this->userSecret)->withHeaders([
            "redirect_uri: http://exemplary.redirect.uri"
        ])->post(
            "https://allegro.pl/auth/oauth/token?grant_type=refresh_token&refresh_token=$this->refreshToken"
        );
    }
}
