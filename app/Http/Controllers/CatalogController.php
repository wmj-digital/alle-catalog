<?php namespace App\Http\Controllers;

use App\Models\AlleAuthenticator;
use App\Models\AlleCatalog;
use App\Models\AlleDownloader;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Laravel\Lumen\Routing\Controller;

class CatalogController extends Controller
{
    const SELF_NAME = 'CatalogController';
    const CATALOG_PAGE_CACHE_PATH = 'catalogPage.html';

    /**
     * @return string
     */
    function __invoke(): string
    {
        return $this->getCatalogPage();
    }

    /**
     * @return string
     * @throws FileNotFoundException
     */
    function getCatalogPage(): string
    {
        return $this->catalogPageCacheExist() ? $this->loadCatalogPageCache() : $this->generateCatalogPageCache();
    }

    /**
     * Adapter method.
     *
     * @return bool
     */
    protected function catalogPageCacheExist(): bool
    {
        return Storage::disk('local')->exists(self::CATALOG_PAGE_CACHE_PATH);
    }

    /**
     * @return string
     */
    function generateCatalogPageCache(): string
    {
        $view = $this->makeView(new AlleCatalog(new AlleDownloader(new AlleAuthenticator())));
        $this->saveCatalogPageCache($view);

        return $view;
    }

    /**
     *
     * Adapter method
     *
     * @param AlleCatalog $catalog
     *
     * @return string
     */
    protected function makeView(AlleCatalog $catalog): string
    {
        return View::make('catalog', [
            'catalog' => $catalog
        ])->render();
    }

    /**
     * Adapter method.
     *
     * @return string
     * @throws FileNotFoundException
     */
    protected function loadCatalogPageCache(): string
    {
        return Storage::disk('local')->get(self::CATALOG_PAGE_CACHE_PATH);
    }

    /**
     * Adapter method.
     *
     * @param string $page
     */
    protected function saveCatalogPageCache(string $page): void
    {
        Storage::disk('local')->put(self::CATALOG_PAGE_CACHE_PATH, $page);
    }

}
